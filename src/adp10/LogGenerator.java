package adp10;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Klasse zum Generieren der Log-Eintraege
 */
public class LogGenerator {
    private final static Charset ENCODING = StandardCharsets.UTF_8;

    public void generate(String fileName, int lines) throws IOException {
        List<String> date = date();
        List<String> time = time();
        List<String> end = end();
        List<String> list = new ArrayList<String>();
        for (int i = 0; i < lines; i++) {
            StringBuffer event1 = new StringBuffer();
            event1.append(String.format("%d.%d.%d.%d - -", randInt(100, 200),randInt(100, 200), randInt(10, 99), randInt(10, 99)));
            event1.append(String.format("[%s/2012%s+ 0200] ", randStr(date),
                            randStr(time)));
            event1.append(String.format("\"GET %s\"", randStr(end)));
            list.add(event1.toString());
        }
        write(fileName, list);
    }

    public Hash<String> read(String fileName) throws IOException {
        Path path = Paths.get(fileName);
        Hash<String> hash = null;
        int key = 0;
        try (BufferedReader reader = Files.newBufferedReader(path, ENCODING)) {
            String line = null;
            hash = new Hash<String>();
            while ((line = reader.readLine()) != null) {
                key = extractKey(line);
                hash.add(key, line);
            }
        }
        return hash;
    }

    public void write(String fileName, List<String> lines) throws IOException {
        Path path = Paths.get(fileName);
        try (BufferedWriter writer = Files.newBufferedWriter(path, ENCODING)) {
            for (String line : lines) {
                writer.write(line);
                writer.newLine();
            }
        }
    }

    private int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    private String randStr(List<String> list) {
        int index = randInt(0, list.size() - 1);
        return list.get(index);
    }

    private List<String> date() {
        String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
                "Aug", "Sep", "Okt", "Nov", "Dec" };
        List<String> list = new ArrayList<String>();
        int n = months.length;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < 10; j++) {
                list.add("0" + j + "/" + months[i]);
            }
            for (int j = 10; j < 29; j++) {
                list.add(j + "/" + months[i]);
            }
        }
        return list;
    }
    
    private List<String> time() {
        List<String> list = new ArrayList<String>();
        for (int i = 1; i < 10; i++) {
            list.add(":08:0" + i + ":00");
        }
        for (int i = 10; i < 61; i++) {
            list.add(":08:" + i + ":00");
        }
        return list;
    }

    private List<String> end() {
        List<String> list = new ArrayList<String>();
        list.add("/pics.html HTTP/1.0");
        list.add("/ HTTP/1.0");
        list.add("/~abc/iks_home.html");
        list.add("/trenner_lila.html");
        list.add("/~abc/porn_home.html");
        list.add("/~abc/cartoon_home.html");
        list.add("/~abc/horror_home.html");
        list.add("/~abc/anime_home.html");
        return list;
    }
    
    private int extractKey(String input) {
        char point = '.';
        int pointCounter = 0;
        char puffer = 0;
        StringBuilder string = new StringBuilder();
        int i = 0;
        do {
            puffer = input.charAt(i);
            if (puffer != point)
                string.append(puffer);
            else
                pointCounter++;
            i++;
        } while (pointCounter < 2);
        return Integer.parseInt(string.toString());
    }
}
