package adp10;

import java.util.ArrayList;

public interface I_GUI {
	ArrayList<String> get(int key);

	ArrayList<Key> getKeySet();
}
