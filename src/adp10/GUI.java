package adp10;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * GUI-Plattform zur Ausgabe
 */
public class GUI<T> extends JFrame {

	private static final long serialVersionUID = -3756158908193415653L;
	
	private JList<String> list;
	private JTextPane text;
	
	private Hash<T> hash;
	private int[] ipKey;
	private String[] ipString;

	public GUI(Hash<T> hashp) {
		super("Hashfunktionen");
		this.hash = hashp;
		setLayout(new BorderLayout());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		initIP();
		list = new JList<>(ipString);
		
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		getContentPane().add(new JScrollPane(list), BorderLayout.WEST);
		list.setBackground(Color.LIGHT_GRAY);
		
		list.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				ArrayList<String> tmp = hash.get(ipKey[list.getSelectedIndex()]);
				String out = "";
				
				for(int i = 0; i < tmp.size(); i++) {
					out += tmp.get(i);
					out += "\n";
				}
				
				text.setText(out);
//				text.setText("" + hash.get(Integer.parseInt(ipString[list.getSelectedIndex()])));
			}
		});
		
		text = new JTextPane();
		text.setPreferredSize(new Dimension(500, 500));
		getContentPane().add(new JScrollPane(text), BorderLayout.CENTER);
		text.setBackground(Color.WHITE);
		
		pack();
		this.setLocationRelativeTo(null);
		setVisible(true);
	}
	
	private void initIP() {
		ipString = new String[hash.getKeySet().size()];
		ipKey = new int[hash.getKeySet().size()];
		for(int i = 0; i < hash.getKeySet().size(); i++) {
			if(hash.getKeySet().get(i) != null) {
				ipString[i] = "" + hash.getKeySet().get(i).getIP();
				ipKey[i] = hash.getKeySet().get(i).getKey();
			}
		}
	}
}
