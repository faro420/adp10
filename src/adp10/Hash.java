package adp10;

import java.util.ArrayList;

/**
 * Custom Hash Datastruktur
 */
public class Hash<T> implements I_GUI {
	private int size;
	private LogfileStruct<T>[] hashTable;
	private ArrayList<Key> keyset;
	private double loadfactor;
	private int numberOf;
	private final double minLoadfactor = 0.5;
	private final double maxLoadfactor = 0.8;
	private final double resizeValue = maxLoadfactor / minLoadfactor;

	@SuppressWarnings("unchecked")
	public Hash()
	{
		this.size = 15;
		this.hashTable = new LogfileStruct[size];
		numberOf = 0;
		keyset = new ArrayList<Key>();
	}
	
	public int numberOf() {
		return numberOf;
	}

	public void add(int key, String data)
    {
        LogfileStruct<T> add = new LogfileStruct<T>(key, data);
        int hash = hashValue3(key, hashTable);
        if(hash >= 0)
        {
        	if(hashTable[hash] == null)
        	{ 
        		Key k = new Key(key, data.substring(0, 13));
        		keyset.add(k);
        		hashTable[hash] = add;
        		numberOf++;
        	}
        	else
        	{
        		hashTable[hash].addData(data);
        	}
        }
      
        loadfactor = (double)numberOf / (double)size;
        if(loadfactor > maxLoadfactor)
        {
        	resize();
        }
       
    }

	public int hashValue1(int key)
	{
		int hashValue = 0;
		hashValue = key % size;
		return hashValue;
	}
	public int hashValue2(int key)
	{
		int hashValue = 0;
		hashValue = 1 + (key % (size-2));
		return hashValue;
	}
	public int hashValue3(int key, LogfileStruct<T>[] hashTable)
	{
		int returnValue = -1;
		int hashValue = 0;
		boolean abbruch = true;
		for(int i = 0; i<5 && abbruch; i++)
		{
			if(i == 0)
			{
				hashValue = key % hashTable.length;
			}
			else
			{
				hashValue = 1  + (key % (hashTable.length - (i*i)));
			}
			if(hashTable[hashValue] == null)
			{
				returnValue = hashValue;
				abbruch = false;
			}
			else
			{
				if(hashTable[hashValue].getKey() ==  key)
				{
					returnValue = hashValue;
					abbruch = false;
				}
			}
		}
		return returnValue;
	}

	@SuppressWarnings("unchecked")
	private void resize() 
	{
		int oldsize = size;
		size = (int) ((size) * resizeValue);
		LogfileStruct<T>[] temp = new LogfileStruct[size];
		for (int i = 0; i < oldsize; i++) 
		{
			if(hashTable[i] != null) {
				int hash = hashValue3(hashTable[i].getKey(), temp);
				if(hash >= 0)
				{
					if(temp[hash] == null)
					{
						temp[hash] = hashTable[i];
					}
					else
					{
						for(int j = 0; j < hashTable[i].getData().size(); j++) {
							temp[hash].addData(hashTable[i].getData().get(j));
						}
						
					}
				}
			}
		}
		hashTable = temp;
	}

	@Override
	public ArrayList<String> get(int key) 
	{
		ArrayList<String> list = new ArrayList<String>();
		int hash = hashValue3(key, hashTable);
		
		if(hash >= 0 && hashTable[hash]!=null)
		{
			list = hashTable[hash].getData();
		}
		
		return list;
	}

	public ArrayList<Key> getKeySet() 
	{
		return keyset;
	}

}
