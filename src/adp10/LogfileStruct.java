package adp10;

import java.util.ArrayList;

public class LogfileStruct<T> {

	private int key;
	private ArrayList<String> data;

	public LogfileStruct(int key, String data) {
		this.key = key;
		this.data =new ArrayList<String>();
		addData(data);
	}

	public void addData(String data) {
		String sdata = (String) data;
		this.data.add(sdata);
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public ArrayList<String> getData() {
		return data;
	}


}
