package adp10;

public class Key {
	private final int key;
	private final String IP;

	public Key(int key, String IP) {
		this.key = key;
		this.IP = IP;
	}

	public int getKey() {
		return key;
	}

	public String getIP() {
		return IP;
	}
}
